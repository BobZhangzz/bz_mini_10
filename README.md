# bz_mini_10
In this project, we are using rust to implement a code that uses hugging face transformers to perform language modeling on a given.

There are several steps to follow to get the code running on your local machine. The steps are as follows:

- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

## Dockerize Hugging Face Rust transformer
To dockerize the Hugging Face Rust transformer, you need to follow the steps below:

1. Clone the repository
2. Build the Docker image
3. Run the Docker container


In the main.rs file, you will find the code that uses the Hugging Face Rust transformer to perform language modeling on a given text.

```rust
/*
hugging face library is used to get sentence embeddings.
 */
use rust_bert::pipelines::sentence_encodings::{SentenceEmbeddingsModelType, SentenceEmbeddingsBuilder};
use bz_mini_10::greetings;

fn main() {
    let model = SentenceEmbeddingsBuilder::remote(
            SentenceEmbeddingsModelType::AllMiniLmL12V2
        ).create_model()?;

    let sentences = [
        "this is an example sentence", 
        "each sentence is converted",
        greetings(),
    ];
    
    let output = model.encode(&sentences)?;
}
```

In the lib.rs file, you will find the code that generates a greeting message.

```rust
pub fn greetings() {
    println!("Hello, world!");
}
```

## Deploy container to AWS Lambda
To deploy the container to AWS Lambda, you need to follow the steps below:

1. Create an ECR repository
2. Push the Docker image to ECR
3. Create an AWS Lambda function
4. Deploy the Docker image to AWS Lambda

### Create an ECR repository
To create an ECR repository, run the command below:

```bash
aws ecr create-repository --repository-name huggingface-rust-transformer
```


