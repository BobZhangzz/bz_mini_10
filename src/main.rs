/*
hugging face library is used to get sentence embeddings.
 */
use rust_bert::pipelines::sentence_encodings::{SentenceEmbeddingsModelType, SentenceEmbeddingsBuilder};
use bz_mini_10::greetings;

fn main() {
    let model = SentenceEmbeddingsBuilder::remote(
            SentenceEmbeddingsModelType::AllMiniLmL12V2
        ).create_model()?;

    let sentences = [
        "this is an example sentence", 
        "each sentence is converted",
        greetings(),
    ];
    
    let output = model.encode(&sentences)?;
}
